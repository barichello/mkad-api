# mkad-api

A sample api to calculate distances using Google's Place and Geocode API.
The destination string is fed to Google's Geocode API which returns a coordinate, if that coordinate is outside the MKAD polygon we calculate its distance using Google's Distance Matrix service and return its response.

# How to run

A Google API Key is required to run this program, you can get it from https://console.cloud.google.com/apis/credentials. The Geocode and Places API need to be enabled in their respective consoles.

```
docker build -t mkad-api .
docker run --rm -e 'MAPS_API_KEY=<your-key-here> mkad-api
```

This commands runs the tests first and then starts the api.

# Request example

`curl 'http://localhost:8000/distance?destination=moscow'`
