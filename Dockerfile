FROM python:3.8
LABEL author="barichello"

COPY . /app
COPY poetry.lock pyproject.toml /app/
WORKDIR /app
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python - \
    && export PATH="/root/.local/bin:$PATH" \
    && poetry config virtualenvs.create false \
    && poetry install

CMD pytest; python3 src/main.py
