#!/usr/bin/python3

"""
API main file.
"""

import requests
import urllib.parse
from typing import Any, Tuple
from pprint import pprint
from shapely.geometry import Point
from flask import Flask, request
from utils import (
    API_PORT,
    DEBUG_PORT,
    FLASK_ENV,
    INSIDE_MKAD_MSG,
    INVALID_INPUT_MSG,
    MAPS_API_KEY,
    MKAD_POLYGON,
    NO_RESULTS,
    SUCCESS_MSG,
)


def create_flask() -> Flask:
    app = Flask(__name__)

    @app.route("/health", methods=["GET"])
    def health() -> Tuple[Any, int]:
        return_code = 200
        return_data = {
            "status": "OK",
        }
        return return_data, return_code

    @app.route("/distance", methods=["GET"])
    def distance() -> Tuple[Any, int]:
        origin = "Moscow Ring Road"
        destination_query = request.args.get("destination", default="")
        if destination_query == "":
            return {"message": INVALID_INPUT_MSG}, 400
        destination_str = urllib.parse.quote_plus(destination_query)

        geocode_url = f"https://maps.googleapis.com/maps/api/geocode/json?address={destination_str}&key={MAPS_API_KEY}"
        geocode_response = requests.get(geocode_url).json()
        if geocode_response["status"] == "ZERO_RESULTS":
            return {"message": NO_RESULTS}, 400

        destination_coordinates = geocode_response["results"][0]["geometry"]["location"]
        destination_point = Point(
            destination_coordinates["lat"], destination_coordinates["lng"]
        )
        if MKAD_POLYGON.contains(destination_point):
            return {"message": INSIDE_MKAD_MSG}, 200

        request_string = f"https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins={origin}&destinations={destination_str}&key={MAPS_API_KEY}"
        distance_response = requests.post(request_string).json()
        distance_txt = distance_response["rows"][0]["elements"][0]["distance"]["text"]
        distance = float(distance_txt.replace("km", ""))
        return {
            "message": SUCCESS_MSG,
            "distance": {"txt": distance_txt, "value": distance},
        }, 200

    return app


def main():
    assert MAPS_API_KEY != None, "A valid MAPS_API_KEY is required!"
    debug = FLASK_ENV == "development"
    if debug:
        run_debugger()
    app = create_flask()
    app.run(host="0.0.0.0", port=API_PORT, debug=debug)


def run_debugger() -> None:
    import ptvsd

    try:
        print(f"Debugger started on port {DEBUG_PORT}", flush=True)
        ptvsd.enable_attach(address=("0.0.0.0", DEBUG_PORT))
    except:
        print("Debug server already running")


if __name__ == "__main__":
    main()
