import json
from main import create_flask
from utils import (
    INSIDE_MKAD_MSG,
    MAPS_API_KEY,
    INVALID_INPUT_MSG,
    NO_RESULTS,
    SUCCESS_MSG,
)


def test_required_envs():
    assert MAPS_API_KEY != None, "A valid MAPS_API_KEY is required!"


def test_health():
    flask = create_flask()
    with flask.test_client() as client:
        response = client.get("/health")
        response_json = json.loads(response.data)
        assert response.status_code == 200
        assert response_json["status"] == "OK"


def test_distance_no_query_str():
    flask = create_flask()
    with flask.test_client() as client:
        response = client.get("/distance")
        response_json = json.loads(response.data)
        assert response.status_code == 400
        assert response_json["message"] == INVALID_INPUT_MSG


def test_distance_empty_query_str():
    flask = create_flask()
    with flask.test_client() as client:
        response = client.get("/distance?destination=")
        response_json = json.loads(response.data)
        assert response.status_code == 400
        assert response_json["message"] == INVALID_INPUT_MSG


def test_distance_invalid_input():
    flask = create_flask()
    with flask.test_client() as client:
        response = client.get("/distance?destination=!!!@@@")
        response_json = json.loads(response.data)
        assert response.status_code == 400
        assert response_json["message"] == NO_RESULTS


def test_distance_inside_mkad():
    flask = create_flask()
    with flask.test_client() as client:
        response = client.get("/distance?destination=ramenki district")
        response_json = json.loads(response.data)
        assert response.status_code == 200
        assert response_json["message"] == INSIDE_MKAD_MSG


def test_distance_outside_mkad():
    flask = create_flask()
    with flask.test_client() as client:
        response = client.get("/distance?destination=podolsk")
        response_json = json.loads(response.data)
        assert response.status_code == 200
        assert response_json["message"] == SUCCESS_MSG
        assert response_json["distance"]["value"] == 46.8
